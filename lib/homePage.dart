import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import './main.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Home Page',
              style: TextStyle(fontWeight: FontWeight.bold)),
          backgroundColor: Colors.yellow,
        ),
        drawer: SideNav(),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        children: const <Widget>[
                          Text("ข่าวสาร",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 32)),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(15, 10, 10, 10),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        children: const <Widget>[
                          Text(
                              "1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย \n ปีการศึกษา 2565(ด่วน)",
                              style: TextStyle(fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: [
                          Center(
                              child: Column(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 300,
                                width: 350,
                                decoration: const BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(""),
                                        fit: BoxFit.cover)),
                              ),
                            ],
                          )),
                        ],
                      ),
                      Row(
                        children: const <Widget>[
                          Text(
                              "**นิสิตต้องยื่นคำร้องขอสำเร็จการศึกษาและชำระเงิน\nตามกำหนดเวลาหากนิสิตไม่ได้ยื่นสำเร็จการศึกษาทาง\nกองทะบียนฯจะไม่เสนอชื่อสำเร็จการศึกษาคู่มือการยื่น\nคำร้องสำเร็จการศึกษานิสิตพิมพ์ใบชำระเงินที่เมนูแจ้งจบ\nและขึ้นทะเบียนนิสิต",
                              style: TextStyle(fontSize: 16)),
                        ],
                      ),
                      const Divider(
                        color: Colors.black26,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(15, 10, 10, 10),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        children: const <Widget>[
                          Text(
                              "2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียน\nภาคปลายปีการศึกษา 2565",
                              style: TextStyle(fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: [
                          Center(
                              child: Column(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 300,
                                width: 350,
                                color: Colors.blue.shade50,
                              ),
                            ],
                          )),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(15, 10, 10, 10),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        children: const <Widget>[
                          Text(
                              "3.กำหนดยื่นคำร้องเกี่ยวกับงานพิธีพระราชทานปริญญา\nบัตรปีการศึกษา 2563 และปีการศึกษา 2564",
                              style: TextStyle(fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: [
                          Center(
                              child: Column(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 300,
                                width: 350,
                                color: Colors.blue.shade50,
                              ),
                            ],
                          )),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(15, 10, 0, 10),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        children: const <Widget>[
                          Text(
                              "4.แบบประเมินความคิดเห็นต่อการให้บริการของ\nสำนักงานอธิการบดี",
                              style: TextStyle(fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: [
                          Center(
                              child: Column(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 300,
                                width: 350,
                                color: Colors.blue.shade50,
                              ),
                            ],
                          )),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}

// Padding(
//                     padding: EdgeInsets.all(10),
//                     child: PageView(
//                         controller: pageViewController ??=
//                             PageController(initialPage: 0),
//                         scrollDirection: Axis.horizontal,
//                         children: [
//                           Padding(
//                             padding:
//                                 EdgeInsetsDirectional.fromSTEB(10, 10, 10, 0),
//                             child: ClipRRect(
//                               borderRadius: BorderRadius.circular(10),
//                               child: Image.network(
//                                 'https://picsum.photos/seed/903/600',
//                                 width: 54.6,
//                                 height: 67.8,
//                                 fit: BoxFit.fitHeight,
//                               ),
//                             ),
//                           ),
//                           Image.network(
//                             'https://picsum.photos/seed/232/600',
//                             width: 100,
//                             height: 100,
//                             fit: BoxFit.cover,
//                           ),
//                         ]),
//                   ),
