import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import './homePage.dart';
import './profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Test App',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('Reg@BUU',
                style: TextStyle(fontWeight: FontWeight.bold)),
            actions: const [
              Icon(Icons.more_vert),
            ],
          ),
          body: const Center(
            child: Text(
              "Test Home",
              style: TextStyle(fontSize: 24),
            ),
          ),
          drawer: SideNav(),
        ),
      ),
    );
  }
}

class SideNav extends StatefulWidget {
  @override
  State<SideNav> createState() => _SideNav();
}

class _SideNav extends State<SideNav> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[buildHeader(context), buildMenuItem(context)],
      ),
    );
  }

  Widget buildHeader(BuildContext context) {
    return const UserAccountsDrawerHeader(
      accountName: Text("Ruankaew Kongkhunthod",
          style: TextStyle(color: Colors.black, fontSize: 18)),
      accountEmail: Text("63160216@go.buu.ac.th",
          style: TextStyle(color: Colors.black, fontSize: 16)),
      currentAccountPicture: CircleAvatar(
        // ignore: sort_child_properties_last
        child: Text(
          "N",
          style: TextStyle(color: Colors.black, fontSize: 32),
        ),
        backgroundColor: Colors.white,
      ),
      decoration: BoxDecoration(
        color: Colors.yellow,
      ),
    );
  }

  Widget buildMenuItem(BuildContext context) {
    return Column(
      // runSpacing: 16,
      children: [
        ListTile(
            leading: const Icon(Icons.home_outlined),
            title: const Text('Home'),
            onTap: () => Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const HomePage()))),
        ListTile(
            leading: const Icon(Icons.account_circle),
            title: const Text('Profile'),
            onTap: () => Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const Profile()))),
        ListTile(
          leading: const Icon(Icons.settings),
          title: const Text('Settings'),
          onTap: () {},
        ),
        const AboutListTile(
          icon: Icon(
            Icons.info,
          ),
          applicationIcon: Icon(Icons.local_play),
          applicationName: "Reg@Buu",
          applicationVersion: '1.0.25',
          applicationLegalese: '@ 2019 company',
          child: Text('Abount Page'),
        )
      ],
    );
  }
}

class _router extends StatelessWidget {
  const _router({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        './homePage.dart': (BuildContext ctx) => const HomePage(),
        './profile.dart': (BuildContext ctx) => const Profile()
      },
    );
  }
}

class _Profile extends StatelessWidget {
  const _Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Test Profile'),
      ),
    );
  }
}
